package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.FoodFactory;

public class SnevnezhaShirataki extends Menu {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami
    public SnevnezhaShirataki(String name, FoodFactory foodFactory){
        super(name, foodFactory);
    }
}