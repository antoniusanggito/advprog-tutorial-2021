package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.FoodFactory;

public class MondoUdon extends Menu {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty
    public MondoUdon(String name, FoodFactory foodFactory){
        super(name, foodFactory);
    }
}