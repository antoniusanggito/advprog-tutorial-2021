package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.FoodFactory;

public class InuzumaRamen extends Menu {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    public InuzumaRamen(String name, FoodFactory foodFactory){
        super(name, foodFactory);
    }
}