package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SobaFactoryTest {
    private Class<?> sobaFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        sobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SobaFactory");
    }

    @Test
    public void testCreateIngredientsReturnCorrectly() {
        SobaFactory factory = new SobaFactory();

        assertTrue(factory.createNoodle() instanceof Soba);
        assertTrue(factory.createMeat() instanceof Beef);
        assertTrue(factory.createTopping() instanceof Mushroom);
        assertTrue(factory.createFlavor() instanceof Sweet);
    }
}
