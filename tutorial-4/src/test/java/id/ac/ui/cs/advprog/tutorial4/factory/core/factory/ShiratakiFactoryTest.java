package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShiratakiFactoryTest {
    private Class<?> shiratakiFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        shiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.ShiratakiFactory");
    }

    @Test
    public void testCreateIngredientsReturnCorrectly() {
        ShiratakiFactory factory = new ShiratakiFactory();

        assertTrue(factory.createNoodle() instanceof Shirataki);
        assertTrue(factory.createMeat() instanceof Fish);
        assertTrue(factory.createTopping() instanceof Flower);
        assertTrue(factory.createFlavor() instanceof Umami);
    }
}
