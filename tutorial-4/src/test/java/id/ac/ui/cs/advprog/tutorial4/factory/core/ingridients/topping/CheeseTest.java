package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Cheese topping = new Cheese();
        assertEquals(topping.getDescription(), "Adding Shredded Cheese Topping...");
    }
}
