package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Soba noodle = new Soba();
        assertEquals(noodle.getDescription(), "Adding Liyuan Soba Noodles...");
    }
}
