package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;

    private OrderDrink orderDrink;
    private OrderFood orderFood;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderDrink = OrderDrink.getInstance();
        orderFood = OrderFood.getInstance();
    }

//    @Test
//    public void testOrderADrinkCorrectlyImplemented() {
//        orderService.orderADrink("Coke");
//        verify(orderDrink, times(1)).setDrink("Coke");
//    }

    @Test
    public void testGetDrinkReturnsOrderDrink() {
        assertTrue(orderService.getDrink() instanceof OrderDrink);
    }

//    @Test
//    public void testOrderAFoodCorrectlyImplemented() {
//        orderService.orderAFood("Fries");
//        verify(orderFood, times(1)).setFood("Fries");
//    }

    @Test
    public void testGetFoodReturnsOrderFood() {
        assertTrue(orderService.getFood() instanceof OrderFood);
    }

}
