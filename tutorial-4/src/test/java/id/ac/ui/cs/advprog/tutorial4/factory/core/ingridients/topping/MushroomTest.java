package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MushroomTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Mushroom topping = new Mushroom();
        assertEquals(topping.getDescription(), "Adding Shiitake Mushroom Topping...");
    }
}
