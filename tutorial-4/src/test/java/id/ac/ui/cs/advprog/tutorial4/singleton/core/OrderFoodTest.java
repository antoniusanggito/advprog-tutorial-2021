package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    private OrderFood orderFood;

    @Test
    public void testGetInstanceReturnsOrderFood() {
        orderFood = OrderFood.getInstance();
        assertTrue(orderFood instanceof OrderFood);
        assertNotNull(orderFood);
    }

//    @Test
//    public void testSetGetFoodCorrectlyImplemented() {
//        orderFood.setFood("Fries");
//        assertEquals("Fries", orderFood.getFood());
//    }

//    @Test
//    public void testToStringCorrectlyImplemented() {
//        assertEquals("Fries", orderFood.toString());
//    }
}
