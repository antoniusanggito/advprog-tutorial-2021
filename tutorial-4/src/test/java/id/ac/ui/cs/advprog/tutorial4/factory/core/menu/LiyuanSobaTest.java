package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SobaFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.SobaTest;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }

    @Test
    public void testIngredientsSetCorrectly() {
        LiyuanSoba menu = new LiyuanSoba("Soba", new SobaFactory());

        assertEquals("Soba", menu.getName());
        assertTrue(menu.getNoodle() instanceof Soba);
        assertTrue(menu.getMeat() instanceof Beef);
        assertTrue(menu.getTopping() instanceof Mushroom);
        assertTrue(menu.getFlavor() instanceof Sweet);
    }
}
