package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {

    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Ramen noodle = new Ramen();
        assertEquals(noodle.getDescription(), "Adding Inuzuma Ramen Noodles...");
    }

}
