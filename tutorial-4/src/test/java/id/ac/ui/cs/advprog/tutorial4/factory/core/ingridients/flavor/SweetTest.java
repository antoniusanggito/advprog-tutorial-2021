package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Sweet flavor = new Sweet();
        assertEquals(flavor.getDescription(), "Adding a dash of Sweet Soy Sauce...");
    }
}
