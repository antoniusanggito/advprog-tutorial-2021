package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Fish meat = new Fish();
        assertEquals(meat.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }
}
