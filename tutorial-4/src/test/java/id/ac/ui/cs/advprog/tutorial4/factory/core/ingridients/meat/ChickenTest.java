package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Chicken meat = new Chicken();
        assertEquals(meat.getDescription(), "Adding Wintervale Chicken Meat...");
    }
}
