package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiratakiTest {

    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Shirataki noodle = new Shirataki();
        assertEquals(noodle.getDescription(), "Adding Snevnezha Shirataki Noodles...");
    }

}
