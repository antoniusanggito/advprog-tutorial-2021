package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpicyTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Spicy flavor = new Spicy();
        assertEquals(flavor.getDescription(), "Adding Liyuan Chili Powder...");
    }
}
