package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RamenFactoryTest {
    private Class<?> ramenFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ramenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenFactory");
    }

    @Test
    public void testCreateIngredientsReturnCorrectly() {
        RamenFactory factory = new RamenFactory();

        assertTrue(factory.createNoodle() instanceof Ramen);
        assertTrue(factory.createMeat() instanceof Pork);
        assertTrue(factory.createTopping() instanceof BoiledEgg);
        assertTrue(factory.createFlavor() instanceof Spicy);
    }
}
