package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Beef meat = new Beef();
        assertEquals(meat.getDescription(), "Adding Maro Beef Meat...");
    }
}
