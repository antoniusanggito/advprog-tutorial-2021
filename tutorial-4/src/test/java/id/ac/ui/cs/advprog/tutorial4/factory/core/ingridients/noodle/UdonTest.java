package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Udon noodle = new Udon();
        assertEquals(noodle.getDescription(), "Adding Mondo Udon Noodles...");
    }
}
