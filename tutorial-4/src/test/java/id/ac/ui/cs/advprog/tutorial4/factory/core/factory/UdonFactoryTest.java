package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UdonFactoryTest {
    private Class<?> udonFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        udonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.UdonFactory");
    }

    @Test
    public void testCreateIngredientsReturnCorrectly() {
        UdonFactory factory = new UdonFactory();

        assertTrue(factory.createNoodle() instanceof Udon);
        assertTrue(factory.createMeat() instanceof Chicken);
        assertTrue(factory.createTopping() instanceof Cheese);
        assertTrue(factory.createFlavor() instanceof Salty);
    }
}
