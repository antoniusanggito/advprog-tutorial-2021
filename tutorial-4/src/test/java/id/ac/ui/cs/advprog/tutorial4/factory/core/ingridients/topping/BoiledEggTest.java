package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        BoiledEgg topping = new BoiledEgg();
        assertEquals(topping.getDescription(), "Adding Guahuan Boiled Egg Topping");
    }
}
