package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {
    private OrderDrink orderDrink;

    @Test
    public void testGetInstanceReturnsOrderDrink() {
        orderDrink = OrderDrink.getInstance();
        assertTrue(orderDrink instanceof OrderDrink);
        assertNotNull(orderDrink);
    }

//    @Test
//    public void testSetGetDrinkCorrectlyImplemented() {
//        orderDrink.setDrink("Coke");
//        assertEquals("Coke", orderDrink.getDrink());
//    }

//    @Test
//    public void testToStringCorrectlyImplemented() {
//        assertEquals("Coke", orderDrink.toString());
//    }
}
