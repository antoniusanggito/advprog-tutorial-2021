package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SobaFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testGetNameCorrectlyImplemented() {
        String name = "Soba";
        Menu menu = new Menu(name, new SobaFactory());
        String result = menu.getName();

        assertEquals(name, result);
    }

    @Test
    public void testGetNoodleCorrectlyImplemented() {
        Menu menu = new Menu("Soba", new SobaFactory());

        assertTrue(menu.getNoodle() instanceof Noodle);
    }

    @Test
    public void testGetMeatCorrectlyImplemented() {
        Menu menu = new Menu("Soba", new SobaFactory());

        assertTrue(menu.getMeat() instanceof Meat);
    }

    @Test
    public void testGetToppingCorrectlyImplemented() {
        Menu menu = new Menu("Soba", new SobaFactory());

        assertTrue(menu.getTopping() instanceof Topping);
    }

    @Test
    public void testGetFlavorCorrectlyImplemented() {
        Menu menu = new Menu("Soba", new SobaFactory());

        assertTrue(menu.getFlavor() instanceof Flavor);
    }
}
