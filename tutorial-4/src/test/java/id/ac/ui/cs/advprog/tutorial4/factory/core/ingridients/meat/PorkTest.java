package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Pork meat = new Pork();
        assertEquals(meat.getDescription(), "Adding Tian Xu Pork Meat...");
    }
}
