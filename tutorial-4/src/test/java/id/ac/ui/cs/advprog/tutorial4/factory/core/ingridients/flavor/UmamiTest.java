package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Umami flavor = new Umami();
        assertEquals(flavor.getDescription(), "Adding WanPlus Specialty MSG flavoring...");
    }
}
