package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.RamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testIngredientsSetCorrectly() {
        InuzumaRamen menu = new InuzumaRamen("Ramen", new RamenFactory());

        assertEquals("Ramen", menu.getName());
        assertTrue(menu.getNoodle() instanceof Ramen);
        assertTrue(menu.getMeat() instanceof Pork);
        assertTrue(menu.getTopping() instanceof BoiledEgg);
        assertTrue(menu.getFlavor() instanceof Spicy);
    }

}
