package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {
    @Test
    public void testGetDescriptionCorrectlyImplemented() {
        Salty flavor = new Salty();
        assertEquals(flavor.getDescription(), "Adding a pinch of salt...");
    }
}
