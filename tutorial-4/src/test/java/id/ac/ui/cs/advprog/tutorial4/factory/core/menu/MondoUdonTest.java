package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.UdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testIngredientsSetCorrectly() {
        MondoUdon menu = new MondoUdon("Udon", new UdonFactory());

        assertEquals("Udon", menu.getName());
        assertTrue(menu.getNoodle() instanceof Udon);
        assertTrue(menu.getMeat() instanceof Chicken);
        assertTrue(menu.getTopping() instanceof Cheese);
        assertTrue(menu.getFlavor() instanceof Salty);
    }
}
