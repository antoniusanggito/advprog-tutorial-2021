package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.ShiratakiFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.ShiratakiTest;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }

    @Test
    public void testIngredientsSetCorrectly() {
        SnevnezhaShirataki menu = new SnevnezhaShirataki("Shirataki", new ShiratakiFactory());

        assertEquals("Shirataki", menu.getName());
        assertTrue(menu.getNoodle() instanceof Shirataki);
        assertTrue(menu.getMeat() instanceof Fish);
        assertTrue(menu.getTopping() instanceof Flower);
        assertTrue(menu.getFlavor() instanceof Umami);
    }
}
