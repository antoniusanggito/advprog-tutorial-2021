package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SobaFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Spy
    private MenuRepository repo;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testInitClass() {
        menuService = new MenuServiceImpl();
        assertEquals(4, menuService.getMenus().size());
    }

    @Test
    public void testCreateMenuReturnsMenuResult() {
        Menu soba = menuService.createMenu("Soba", "LiyuanSoba");

        assertEquals("Soba", soba.getName());
    }

    @Test
    public void testGetMenusCallsRepoGetMenus() {
        assertEquals(repo.getMenus(), menuService.getMenus());
        verify(repo, atLeastOnce()).getMenus();
    }

    @Test
    public void testMenuServiceHasInitRepoMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("initRepo");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));
    }

}
