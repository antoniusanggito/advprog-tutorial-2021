# Singleton

## Lazy Instantiation  
Pembuatan objek class dilakukan pada global access method. Dalam pembuatan akan dicek terlebih dahulu apakah instance null atau tidak, jika null maka akan diinstansiasi. Keuntungannya adalah menghindari pembuatan objek di awal (jika tidak dibutuhkan langsung). Tapi karena dibuat saat getInstance() dipanggil maka dapat terjadi masalah terkait multithread environment yang harus diatasi dengan menambahkan blok synchronize yang mungkin akan mengurangi performance.

## Eager Instantiation  
Pembuatan objek class secara langsung saat pembuatan static variabel/saat class loading, tidak menunggu getInstance() dipanggil. Sifatnya adalah thread safe yang berarti dipastikan objek class yang akan dipanggil adalah yang sudah diinstansiasi di awal tadi. Namun kerugiannya adalah terkait memori karena objek dibuat sejak awal loading program. Tepat digunakan jika yakin objek singleton akan segera digunakan.