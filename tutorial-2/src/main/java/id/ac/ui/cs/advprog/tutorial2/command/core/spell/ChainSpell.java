package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private static ArrayList<Spell> sealList;

    public ChainSpell(ArrayList<Spell> sealList) {
        this.sealList = sealList;
    }

    @Override
    public void cast() {
        // Iterate through sealList to cast
        for (Spell spell : sealList) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // Iterate from the end of sealList to undo
        for (int i = sealList.size() - 1; i >= 0; i--) {
            sealList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
