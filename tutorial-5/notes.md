# Requirement Tutorial-5:

Entity dan relasi secara berurut: Mata Kuliah (1:N) Mahasiswa (1:N) Log

## /mahasiswa
GET: Menampilkan list mahasiswa
POST: Mendaftarkan mahasiswa dengan @requestbody Mahasiswa

## /mahasiswa/{npm}
GET: Menampilkan mahasiswa dengan npm terkait
PUT: Mengupdate mahasiswa dengan npm terkait
DELETE: Menghapus mahasiswa dengan npm terkait

## /mahasiswa/{npm}/{month}
GET: Menampilkan log summary bulan tertentu dari mahasiswa npm terkait

## /mahasiswa/{npm}/kode_matkul}
POST: Mendaftarkan mahasiswa dengan npm terkait menjadi asdos mata kuliah terkait

## mata-kuliah
GET: Menampilkan list mata kuliah
POST: Mendaftarkan mahasiswa dengan @requestbody Mahasiswa

## /mahasiswa/kode_matkul}
GET: Menampilkan matkul dengan kode terkait
PUT: Mengupdate matkul dengan kode terkait
DELETE: Menghapus matkul dengan kode terkait

## /log
GET: Menampilkan list semua log

## /log/{npm}
POST: Membuat log baru dari mahasiswa

## /log/{idLog}
DELETE: Menghapus log terkait