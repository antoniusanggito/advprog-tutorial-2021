package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;

public interface LogService {
    Log createLog(String npm, Log log);

    Iterable<Log> getListLog();

    void deleteLog(int idLog);
}
