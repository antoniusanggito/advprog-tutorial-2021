package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }

    @Override
    public LogSummary getLogReport(Mahasiswa mahasiswa, int month) {
        LogSummary logSummary = new LogSummary(month);
        for (Log log: mahasiswa.getLogSet()) {
            if (log.getStartTime() != null) {
                LocalDate localDate = log.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if (month == localDate.getMonthValue()) {
                    long jamKerja = (log.getEndTime().getTime() - log.getStartTime().getTime()) / (60 * 60 * 1000);
                    long pembayaran = 350 * jamKerja;
                    logSummary.setJamKerja(logSummary.getJamKerja() + jamKerja);
                    logSummary.setPembayaran(logSummary.getPembayaran() + pembayaran);
                }
            }
        }
        return logSummary;
    }

    @Override
    public Mahasiswa registerAsdos(Mahasiswa mahasiswa, String kode_matkul) {
        mahasiswa.setMataKuliah(mataKuliahRepository.findByKodeMatkul(kode_matkul));
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }
}
