package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(String npm, Log log) {
        log.setMahasiswa(mahasiswaRepository.findByNpm(npm));
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

//    @Override
//    public Log updateLog(String idLog, Log log) {
//        log.setIdLog(idLog);
//        logRepository.save(log);
//        return log;
//    }

    @Override
    public void deleteLog(int idLog) {
        logRepository.deleteById(idLog);
    }
}
