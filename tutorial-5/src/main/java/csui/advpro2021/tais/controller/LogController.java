package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @PostMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

//    @PutMapping(path = "/{idLog}", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
//        return ResponseEntity.ok(logService.updateLog(idLog, log));
//    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
