package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_log", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "start_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @Column(name = "end_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="npm")
    @JsonBackReference
    private Mahasiswa mahasiswa;

    public Log(int idLog, Date startTime, Date endTime, String deskripsi) {
        this.idLog = idLog;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}
