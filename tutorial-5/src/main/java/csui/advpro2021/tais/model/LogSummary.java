package csui.advpro2021.tais.model;

import lombok.*;

@Getter
@Setter
public class LogSummary {

    private int month;
    private long jamKerja;
    private long pembayaran;

    public LogSummary(int month, long jamKerja, long pembayaran) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }

    public LogSummary(int month) {
        this(month, 0, 0);
    }
}
