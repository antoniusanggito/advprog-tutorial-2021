package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;

    @BeforeEach
    public void setUp() throws ParseException {
        Mahasiswa mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date start = format.parse("2020-08-04 05:00:00");
        Date endTime = format.parse("2020-08-04 05:00:00");
        log = new Log(1, start, endTime, "deskripsi");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListLog() throws Exception {
        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(log.getIdLog()));
    }

    @Test
    void testControllerCreateLog() throws Exception {
        when(logService.createLog("1906192052", log)).thenReturn(log);

        mvc.perform(post("/log/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(jsonPath("$.deskripsi").value(log.getDeskripsi()));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception{
        mvc.perform(get("/mata-kuliah/BASDEAD").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerDeleteLog() throws Exception {
        logService.createLog("1906192052", log);
        mvc.perform(delete("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}