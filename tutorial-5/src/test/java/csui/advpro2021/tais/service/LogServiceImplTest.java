package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaService mahasiswaService;

    @Mock
    private MataKuliahService mataKuliahService;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    private Mahasiswa mahasiswa;

    private MataKuliah matkul;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("19012345");
        mahasiswa.setNama("Maung");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("0811111111");

        log = new Log();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date start = format.parse("2020-08-04 05:00:00");
        Date endTime = format.parse("2020-08-04 10:00:00");
        log.setStartTime(start);
        log.setEndTime(endTime);
        log.setDeskripsi("deskripsi");

        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
    }

    @Test
    void testServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    void testServiceCreateLog() {
//        mahasiswaService.createMahasiswa(mahasiswa);
//        Log resultLog = logService.createLog("19012345", log);
//        Assertions.assertNotEquals(log.getIdLog(), 0);
    }

    @Test
    void testServiceDeleteLog() {
        int id = log.getIdLog();
        logService.deleteLog(log.getIdLog());

        assertNotEquals(null, logService.getListLog());
    }
}
