package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private final Spellbook spellbook;
    private boolean isRecharging;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        // Initialize isRecharging value to false
        this.isRecharging = false;
    }

    @Override
    public String normalAttack() {
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        // Inverse isRecharging value when chargedAttack
        isRecharging = !isRecharging;
        return spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

    // Create getter to isRecharging
    public boolean getIsRecharging() {
        return isRecharging;
    }
}
