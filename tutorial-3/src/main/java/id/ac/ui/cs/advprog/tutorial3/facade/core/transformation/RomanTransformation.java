package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini melakukan cipher caesar
 */
public class RomanTransformation implements Transformation{
    private int key;

    public RomanTransformation(int key){
        this.key = key;
    }

    public RomanTransformation(){
        this(13);
    }

    @Override
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    @Override
    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? key % 26 + 26: (26 - key) % 26 + 26;
        int n = text.length();
        char[] res = new char[n];
        for (int i = 0; i < n; i++) {
            if (Character.isLetter(text.charAt(i))) {
                if (Character.isUpperCase(text.charAt(i))) {
                    char ch = (char)(((int)text.charAt(i) + selector - 65) % 26 + 65);
                    res[i] = ch;
                } else {
                    char ch = (char)(((int)text.charAt(i) + selector - 97) % 26 + 97);
                    res[i] = ch;
                }
            } else {
                res[i] = text.charAt(i);
            }
        }
        return new Spell(new String(res), codex);
    }
}
