package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.RomanTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

/**
 * Facade class implementation
 */
public class FacadeMain {

    private final Transformation celestialTransformation;
    private final Transformation abyssalTransformation;
    private final Transformation romanTransformation;
    private final List<Transformation> transformationList;

    public FacadeMain() {
        this.celestialTransformation = new CelestialTransformation();
        this.abyssalTransformation = new AbyssalTransformation();
        this.romanTransformation = new RomanTransformation();
        this.transformationList = new ArrayList<>();
        init();
    }

    private void init() {
        // Add transformations to list
        transformationList.add(celestialTransformation);
        transformationList.add(abyssalTransformation);
        transformationList.add(romanTransformation);
    }

    public Spell encode(Spell spell) {
        for (Transformation transformation : transformationList) {
            spell = transformation.encode(spell);
        }
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        return spell;
    }

    public Spell decode(Spell spell) {
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        for (int i = transformationList.size() - 1; i >= 0; i--) {
            spell = transformationList.get(i).decode(spell);
        }
        return spell;
    }
}

