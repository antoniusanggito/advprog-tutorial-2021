package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    private static boolean isStartUp = true;

    // feel free to include more repositories if you think it might help :)
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Override
    public List<Weapon> findAll() {
        // Get all weapons saved
        if (isStartUp) initWeapons();
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        // Get weapon object with given weaponName and do corresponding attackType
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        if (weaponName.equals("The Windjedi") || weaponName.equals("Heat Bearer")) {
            SpellbookAdapter spellbook = (SpellbookAdapter)weapon;
            if (spellbook.getIsRecharging()) {
                spellbook.chargedAttack();
                return;
            }
        }
        String attackTypeStr = "";
        String attackDesc = "";
        if (attackType == 1) {
            attackTypeStr = "normal attack";
            attackDesc = weapon.normalAttack();
        } else if (attackType == 2) {
            attackTypeStr = "charged attack";
            attackDesc = weapon.chargedAttack();
        }
        logRepository.addLog(String.format("%s attacked with %s (%s): %s",
                weapon.getHolderName(), weapon.getName(), attackTypeStr, attackDesc));
    }

    @Override
    public List<String> getAllLogs() {
        // Get list of logs saved
        return logRepository.findAll();
    }

    @Override
    public void initWeapons() {
        for (Bow bow : bowRepository.findAll()) {
            weaponRepository.save(new BowAdapter(bow));
        }
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }
        isStartUp = false;
    }
}
