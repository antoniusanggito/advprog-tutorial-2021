package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private final Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        // Initialize isAimShot value to false
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        // Inverse isAimShot value when chargedAttack
        isAimShot = !isAimShot;
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
