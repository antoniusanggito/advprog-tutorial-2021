package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class FacadeMainTest {
    private Class<?> facadeMainClass;

    @BeforeEach
    public void setup() throws Exception {
        facadeMainClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.FacadeMain");
    }

    @Test
    public void testFacadeMainIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(facadeMainClass.getModifiers()));
    }

    @Test
    public void testFacadeMainHasInitMethod() throws Exception {
        Method translate = facadeMainClass.getDeclaredMethod("init");
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPrivate(methodModifiers));
    }

    @Test
    public void testFacadeMainHasEncodeMethod() throws Exception {
        Method translate = facadeMainClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeMainHasDecodeMethod() throws Exception {
        Method translate = facadeMainClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

}
