package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RomanTransformationTest {
    private Class<?> romanClass;

    @BeforeEach
    public void setup() throws Exception {
        romanClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.RomanTransformation");
    }

    @Test
    public void testRomanHasEncodeMethod() throws Exception {
        Method translate = romanClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRomanEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";

        Spell result = new RomanTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testRomanEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Vdilud dqg L zhqw wr d eodfnvplwk wr irujh rxu vzrug";

        Spell result = new RomanTransformation(3).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testRomanHasDecodeMethod() throws Exception {
        Method translate = romanClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRomanDecodesCorrectly() throws Exception {
        String text = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new RomanTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testRomanDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "Vdilud dqg L zhqw wr d eodfnvplwk wr irujh rxu vzrug";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new RomanTransformation(3).decode(spell);
        assertEquals(expected, result.getText());
    }
}
