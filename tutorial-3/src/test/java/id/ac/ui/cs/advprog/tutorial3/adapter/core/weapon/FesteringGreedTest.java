package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class FesteringGreedTest {
    private Class<?> festeringGreedClass;

    @BeforeEach
    public void setUp() throws Exception {
        festeringGreedClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.FesteringGreed");
    }

    @Test
    public void testFesteringGreedIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(festeringGreedClass.getModifiers()));
    }

    @Test
    public void testFesteringGreedIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(festeringGreedClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testFesteringGreedOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = festeringGreedClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testFesteringGreedOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = festeringGreedClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testFesteringGreedOverrideGetNameMethod() throws Exception {
        Method getName = festeringGreedClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testFesteringGreedOverrideGetHolderMethod() throws Exception {
        Method getHolderName = festeringGreedClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // Add tests to methods' implementation
    @Test
    public void testNormalAttackCorrectlyImplemented() {
        FesteringGreed festeringGreed = new FesteringGreed("Potty");
        String result = festeringGreed.normalAttack();

        assertEquals("Hayolo torment!", result);
    }

    @Test
    public void testChargedAttackCorrectlyImplemented() {
        FesteringGreed festeringGreed = new FesteringGreed("Potty");
        String result = festeringGreed.chargedAttack();

        assertEquals("Everlasting mental pain", result);
    }

    @Test
    public void testGetHolderNameCorrectlyImplemented() {
        String holderName = "Potty";
        FesteringGreed festeringGreed = new FesteringGreed(holderName);
        String result = festeringGreed.getHolderName();

        assertEquals(holderName, result);
    }
}
