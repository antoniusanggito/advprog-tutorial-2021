package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        // Register to its guild subject
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        // Add quest when type is delivery or rumble
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("R")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
