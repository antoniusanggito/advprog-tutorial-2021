package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        // Register to its guild subject
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        // Add quest for every quest type available
        this.getQuests().add(guild.getQuest());
    }
}
