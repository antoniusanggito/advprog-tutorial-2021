package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        // Initialize each adventurer object
        this.agileAdventurer = new AgileAdventurer(guild);
        this.knightAdventurer = new KnightAdventurer(guild);
        this.mysticAdventurer = new MysticAdventurer(guild);
    }

    @Override
    public void addQuest(Quest quest) {
        // Add quest to repository and
        if (quest.getTitle().length() > 0) {
            questRepository.save(quest);
            guild.addQuest(quest);
        }
    }

    @Override
    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    }
}
