package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        // Register to its guild subject
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        // Add quest when type is delivery or escort
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("E")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
